resource "digitalocean_domain" "default" {
  name       = var.domain_name
}

resource "digitalocean_domain" "shortlink_demo" {
  name       = var.domain_name_2
}
